var spawn = require('child_process').spawn;
var path = require('path');
var extend = require('util')._extend;
var config = require('./configure');
var log = require('winston');
var async = require('async');
module.exports = function(options, cb)
{

	log.info('Running Install for dotnet-console app: %s', options.project);

	config.set(options, function(er,r){
		if(options.install && options.install.serviceExe){
			var pushDirectory = process.cwd();
			//change to target dir if supplied.
			process.chdir(options.target ||process.cwd());
			return runInstallProcess(options, function(err, r){
				process.chdir(pushDirectory);
				cb(err,r)
			});
		}
		else {
			return cb(null,options);
		}
	});
}

function runInstallProcess(options, cb)
{
	var expander = options.fn.variableExpander.resolver({name:  options.name || options.project, environment: options.environment || 'Default' });

	options.install = options.install || {};

	var serviceName = expander.resolve(options.install.serviceName || "${environment}-${name}");

	var env = {
		SERVICE_NAME: serviceName,
		NSSM_EXE: path.join(path.resolve(__dirname), "nssm.exe" ) ,
		SERVICE_EXE_NAME: path.resolve(options.install.serviceExe),
	};

	async.waterfall([
		async.apply(runPreInstall, {env:env}, options),
		function(env, options, cb) {
			setTimeout(function() {
				runInstall(env, options, cb)
			}, 10000); //wait for 10 secs before installing the service
		}
	], cb);

}

function runPreInstall(env, options, cb)
{
	console.log('Running pre-install.Bat with env:', env);
	var cmd = spawn(path.resolve( path.join(__dirname, 'pre-install.bat')), [], env);
	var hasError;
  var errorText = '';

	cmd.stdout.on('data', (data) => {
		log.verbose(data.toString());
	});

	cmd.stderr.on('data', (data) => {
		hasError = true;
		errorText += data.toString() + '\n';
		log.error(data.toString());
	});

	cmd.on('close', (code) => {
		//ignore can't open service error.
		if(hasError && errorText.indexOf("Can't open service") < 0) return cb('An error occured during the "npm run install" command.');
		return cb(null, env, options);
	});
}

function runInstall(env, options, cb)
{
	console.log('Running Instal.Bat with env:', env);
	var cmd = spawn(path.resolve( path.join(__dirname, 'install.bat')), [], env);
	var hasError;

	cmd.stdout.on('data', (data) => {
		log.verbose(data.toString());
	});

	cmd.stderr.on('data', (data) => {
		hasError = true;
		log.error(data.toString());
	});

	cmd.on('close', (code) => {
		if(hasError) return cb('An error occured during the "npm run install" command.');
		return cb(null, options);
	});
}
