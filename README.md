Dnx Console Plugin

This plugin providers the package, publish, configure, install functions for dnx-console apps.

### How do I get set up? ###

To get the plugin installed onto you machine run the following command.
```
npm install git+ssh://git@bitbucket.org:stickytickets/prazzle-package-dnx-console.git -g
```

Once install you can run prazzle to get detailed information about each command.
prazzle

### Developing ###

If your making changes to this project and want to test them out locally you can use npm link.

First make sure you uninstall the plugin from the global package repository
```
    npm rm --global prazzle-package-dnx-console
```
Then go into the plugin directory (the working directory where you'll be make the changes) and run npm link
```
 npm link
```

This should setup the link from the global repository to the current directory, you can verify by running
```
npm ls --global prazzle-package-dnx-console
```

Then go to the directory where you are testing the plugin and run 
```
npm link prazzle-package-dnx-console
```