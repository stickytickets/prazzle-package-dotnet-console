var spawn = require('child_process').spawn;
var async = require('async');
var tmp = require('tmp');
var fs = require('fs');
var path = require('path');
var rimraf = require('rimraf');
var extend = require('util')._extend;
var xmlParseString = require('xml2js').parseString;
var jsxml = require("node-jsxml");
var crlf = require('crlf');
var log = require('winston');

exports.get = function(options, cb)
{
	log.verbose('Reading environment keys...');
	options.env =  options.env || {};

	if(fs.existsSync("Environment.config")){
		var xml = fs.readFileSync("Environment.config");
		xmlParseString(xml, function (err, result) {

			if(result.appSettings && result.appSettings.add){
				for(var i=0; i < result.appSettings.add.length; i++){
					var setting = result.appSettings.add[i]['$'];
					options.env[setting.key]  = setting.value;
				}
			}
			cb(null, options.env );
		});
	}
	else{

			cb(null, options.env );
	}

}

var jsxml = require("node-jsxml");

module.exports.set = function(options, cb)
{
	if(!options.env) return cb();

	var xml = "<appSettings></appSettings>";
	if(fs.existsSync("Environment.config")){
		xml = fs.readFileSync("Environment.config").toString();
	}

	var xml = new jsxml.XML(xml);

	var settings = xml.descendants('add');

	settings.each(function(item, index) {
			var key = item.attribute('key').getValue();
			if (options.env[key]) {
					item.attribute('value').setValue(options.env[key]);
			}
	});

	fs.writeFileSync("Environment.config", xml.toXMLString() );

	crlf.set("Environment.config", 'CRLF', function(err, endingType) {
	  		cb();
	});

}
